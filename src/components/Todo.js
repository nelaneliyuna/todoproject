import React from "react";
import { connect } from "react-redux";
import cx from "classnames";
import { toggleTodo } from "../redux/actions";

const Todo = ({ todo, toggleTodo }) => (
  <li className="todo-item" onClick={() => toggleTodo(todo.id)}>
    {todo && todo.status == 1 ? "👌" : "👋"}{" "}
    <span
      className={cx(
        "todo-item__text",
        todo && todo.status == 1 && "todo-item__text--completed"
      )}
    >
      {todo.title ? todo.title : todo.id.title}
    </span>
  </li>
);

export default connect(
  null,
  { toggleTodo }
)(Todo);
