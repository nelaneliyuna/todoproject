import { ADD_TODO, TOGGLE_TODO, SET_FILTER } from "./actionTypes";

let nextTodoId = 6;

export const addTodo = title=> ({
  type: ADD_TODO,
  payload: {
    id: ++nextTodoId,
    title,
    status: 0
  }
});

export const toggleTodo = id => ({
  type: TOGGLE_TODO,
  payload: { id }
});

export const setFilter = filter => ({ type: SET_FILTER, payload: { filter } });
