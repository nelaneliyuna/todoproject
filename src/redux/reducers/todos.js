import { ADD_TODO, TOGGLE_TODO } from "../actionTypes";
import axios from "axios";

const initialState = {
  allIds: [],
  byIds: {}
};

export function fetchTodo(){
  axios.get(`https://virtserver.swaggerhub.com/hanabyan/todo/1.0.0/to-do-list`)
    .then(res => {
      console.log('data', res)
      const todss = res.data;
      initialState.allIds = todss
    })
}

fetchTodo()

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_TODO: {
      const { id, title } = action.payload;
      return {
        ...state,
        allIds: [...state.allIds, id],
        byIds: {
          ...state.byIds,
          [id]: {
            title,
            status: 0
          }
        }
      };
    }
    case TOGGLE_TODO: {
      const { id } = action.payload;
      return {
        ...state,
        byIds: {
          ...state.byIds,
          [id]: {
            ...state.byIds[id],
            status: !state.byIds[id].status
          }
        }
      };
    }
    default:
      return state;
  }
}
