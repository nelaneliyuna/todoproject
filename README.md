Hello this is Nela's project for Majoo Test Case

You can run this project on your local environment by following these steps:

1. Clone project on your local using: `git remote add origin https://gitlab.com/nelaneliyuna/todoproject.git`
2. Open folder of cloned project, by default the name of folder is todoProject
3. install the project using `npm install`
4. run the project using `npm start`
5. open the running project on browser `http://localhost:3000/`

enjoy and have fun :)
